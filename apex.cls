ContentVersion file = [SELECT Id, Title FROM ContentVersion Where ContentDocumentId = 'your required document id'][0];
insert new ContentDistribution(
   Name = file.Title,
   ContentVersionId = file.Id,
   PreferencesAllowViewInBrowser= true
);
ContentDistribution cd = [SELECT id,ContentDownloadUrl from ContentDistribution WHERE ContentVersionId = file.Id];

String publicUrlYouCanShare = cd.ContentDownloadUrl;